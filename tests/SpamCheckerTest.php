<?php

namespace App\Tests;

use App\Entity\Comment;
use App\Service\SpamChecker;
use Generator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class SpamCheckerTest extends TestCase
{
    /**
     * @dataProvider getCommentsAndKeys
     */
    public function testSpamScoreSpamAdressAndKeyModulo2Is0(int $expectedScore, Comment $comment, string $key): void
    {
        $client = new MockHttpClient([new MockResponse(
            'toto',
            ['response_headers' => ['x-toto-debug-help: Invalid key']]
        )]);
        $spamChecker = new SpamChecker($client, $key);

        $this->assertEquals($expectedScore, $spamChecker->getSpamScore($comment));
    }

    /**
     * @return Generator<string, mixed[]>
     */
    public function getCommentsAndKeys(): Generator
    {
        $comment = new Comment();
        $comment->setCreatedAtValue();
        $comment->setEmail('AGoodMail@mail.test');
        $key = "2";
        yield 'Goodmail, goodkey' => [0, $comment, $key];

        $comment = new Comment();
        $comment->setCreatedAtValue();
        $comment->setEmail('AGoodMail@mail.test');
        $key = "1";
        yield 'Goodmail, badkey' => [2, $comment, $key];

        $comment = new Comment();
        $comment->setCreatedAtValue();
        $comment->setEmail('spam_bademail@mail.test');
        $key = "2";
        yield 'Badmail, goodkey' => [2, $comment, $key];

        $comment = new Comment();
        $comment->setCreatedAtValue();
        $comment->setEmail('spam_bademail@mail.test');
        $key = "1";
        yield 'Badmail, badkey' => [2, $comment, $key];

        $comment = new Comment();
        $comment->setCreatedAtValue();
        $key = "1";
        yield 'Nomail, badkey' => [2, $comment, $key];
    }
}
