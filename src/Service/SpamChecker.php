<?php

namespace App\Service;

use App\Entity\Comment;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class SpamChecker
{
    private HttpClientInterface $client;
    private int $key;

    public function __construct(HttpClientInterface $client, string $key)
    {
        $this->client = $client;
        $this->key = (int)$key;
    }

  /**
     * @return int Spam score: 0: not spam, 1: maybe spam, 2: blatant spam
     * Can be done with an http request follow book
     */
    public function getSpamScore(Comment $comment): int
    {
        $response = $this->client->request('GET', "https://google.fr");
        return $this->key % 2 === 0 && !str_contains($comment->getEmail() ?? "", 'spam') ? 0 : 2;
    }
}
