<?php

namespace App\Message;

class CommentMessage
{
    private int $id;
    private string $reviewUrl;
    /**
     * @var mixed[] $context
     */
    private array $context;

    /**
     * @param mixed[] $context
     */
    public function __construct(int $id, string $reviewUrl, array $context = [])
    {
        $this->id = $id;
        $this->reviewUrl = $reviewUrl;
        $this->context = $context;
    }

    /**
     * Get the value of id
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Get the value of context
     * @return mixed[]
     */
    public function getContext(): array
    {
        return $this->context;
    }

    /**
     * Get the value of reviewUrl
     */
    public function getReviewUrl(): string
    {
        return $this->reviewUrl;
    }
}
