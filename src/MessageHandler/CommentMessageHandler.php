<?php

namespace App\MessageHandler;

use App\Message\CommentMessage;
use App\Notification\CommentReviewNotification;
use App\Repository\CommentRepository;
use App\Service\ImageOptimizer;
use App\Service\SpamChecker;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\Mime\NotificationEmail;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Notifier\Notifier;
use Symfony\Component\Notifier\NotifierInterface;
use Symfony\Component\Workflow\WorkflowInterface;

class CommentMessageHandler implements MessageHandlerInterface
{
    private SpamChecker $spamChecker;
    private EntityManagerInterface $entityManager;
    private CommentRepository $commentRepository;
    private MessageBusInterface $bus;
    private WorkflowInterface $workflow;
    private NotifierInterface $notifier;
    private ImageOptimizer $imageOptimizer;
    private string $photoDir;
    private ?LoggerInterface $logger;

    public function __construct(
        EntityManagerInterface $entityManager,
        SpamChecker $spamChecker,
        CommentRepository $commentRepository,
        MessageBusInterface $bus,
        WorkflowInterface $commentStateMachine,
        NotifierInterface $notifier,
        ImageOptimizer $imageOptimizer,
        string $photoDir,
        LoggerInterface $logger = null
    ) {
        $this->spamChecker = $spamChecker;
        $this->entityManager = $entityManager;
        $this->commentRepository = $commentRepository;
        $this->bus = $bus;
        $this->workflow = $commentStateMachine;
        $this->notifier = $notifier;
        $this->imageOptimizer = $imageOptimizer;
        $this->photoDir = $photoDir;
        $this->logger = $logger;
    }

    public function __invoke(CommentMessage $message): void
    {
        $comment = $this->commentRepository->find($message->getId());
        if (!$comment) {
            return;
        }

        // if (2 === $this->spamChecker->getSpamScore($comment)) {
        //     $comment->setState('spam');
        // } else {
        //     $comment->setState('published');
        // }

        // $this->entityManager->flush();
        $canChangeToAccept = $this->workflow->can($comment, 'accept');

        $canChangeToPublish = $this->workflow->can($comment, 'publish');
        $canChangeToPublishHam = $this->workflow->can($comment, 'publish_ham');

        if ($canChangeToAccept) {
            $score = $this->spamChecker->getSpamScore($comment);
            $transition = 'accept';
            if (2 === $score) {
                $transition = 'reject_spam';
            } elseif (1 === $score) {
                $transition = 'might_be_spam';
            }
            $this->workflow->apply($comment, $transition);
            $this->entityManager->flush();

            $this->bus->dispatch($message);
        } elseif ($canChangeToPublish || $canChangeToPublishHam) {
            // $transitionPublish = $canChangeToPublish ? 'publish' : 'publish_ham';
            // $this->workflow->apply($comment, $transitionPublish);
            // $this->mailer->send(
            //     (new NotificationEmail())
            //     ->subject('New comment submitted')
            //     ->htmlTemplate('emails/comment_notification.html.twig')
            //     ->from($this->adminEmail)
            //     ->to($this->adminEmail)
            //     ->context(['comment' => $comment])
            // );
            /** @var Notifier $emailNotifier */
            $emailNotifier = $this->notifier;
            $this->notifier->send(
                new CommentReviewNotification($comment, $message->getReviewUrl()),
                ...$emailNotifier->getAdminRecipients()
            );
        } elseif ($this->workflow->can($comment, 'optimize')) {
            if ($comment->getPhotoFilename()) {
                $this->imageOptimizer->resize($this->photoDir . '/' . $comment->getPhotoFilename());
            }
            $this->workflow->apply($comment, 'optimize');
            $this->entityManager->flush();
        } elseif ($this->logger) {
            $this->logger->debug(
                'Dropping comment message',
                ['comment' => $comment->getId(), 'state' => $comment->getState()]
            );
        }
    }
}
