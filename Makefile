SHEL := /bin/bash

tests: export APP_ENV=test
tests:
	symfony console doctrine:database:drop --force || true
	symfony console doctrine:database:create
	symfony console doctrine:migrations:migrate -n
	symfony console doctrine:fixtures:load -n
	XDEBUG_MODE=coverage ./vendor/bin/simple-phpunit --coverage-html var/html
.PHONY: tests

analyze:
	vendor/bin/phpcbf --ignore=*config/secrets/*
	vendor/bin/phpcs --ignore=*config/secrets/*
	php -d memory_limit=4G vendor/bin/phpstan analyse --no-progress
.PHONY: analyze